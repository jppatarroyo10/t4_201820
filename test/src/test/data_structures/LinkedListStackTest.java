package test.data_structures;


import model.data_structures.LinkedListStack;
import junit.framework.TestCase;

public class LinkedListStackTest extends TestCase
{
	private LinkedListStack<Integer> pila;
	
	public void setupEscenario1(){
		pila = new LinkedListStack<Integer>();
		for(int i=0; i<50; i++){
			pila.push(i);
		}
	}
	
	public void testIsEmpty1(){
		setupEscenario1();
		assertTrue(!pila.isEmpty());
	}
	
	public void testIsEmpty2(){
		pila=new LinkedListStack<Integer>();
		assertTrue(pila.isEmpty());
	}
	
	public void testSize1(){
		setupEscenario1();
		assertEquals(50, pila.size());
	}
	
	public void testSize2(){
		setupEscenario1();
		for(int i=0; i<10; i++){
			pila.pop();
		}
		pila.push(100);
		assertEquals(41, pila.size());
	}
	
	public void testPush1(){
		setupEscenario1();
		pila.push(3);
		pila.push(5);
		pila.push(7);
		assertEquals(new Integer(7), pila.getTop());	
	}
	
	public void testPush2(){
		setupEscenario1();
		pila.pop();
		for(int i=50; i<100; i++){
			pila.push(i);
		}
		pila.pop();
		pila.pop();
		assertEquals(new Integer(97), pila.getTop());	
	}
	
	public void testPop1(){
		setupEscenario1();
		for(int i=0; i<10; i++){
			pila.pop();
		}
		assertEquals(new Integer(39), pila.pop());
	}
	
	public void testPop2(){
		setupEscenario1();
		for(int i=0; i<10; i++){
			pila.pop();
		}
		for(int i=40; i<46; i++){
			pila.push(i);
		}
		assertEquals(new Integer(45), pila.pop());
	}
	
	public void testGetTop(){
		setupEscenario1();
		for(int i=0; i<10; i++){
			pila.pop();
		}
		assertEquals(new Integer(39), pila.getTop());
	}
	
	
}
