package model.logic;

//en vez de cambiar los nodos, cambiar los items, un metodo en la clase lista que reciba por parametro dos nodos.
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.*;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.LinkedListQueue;
import model.data_structures.LinkedListStack;
import model.data_structures.Node;

public class DivvyTripsManager implements IDivvyTripsManager {


	LinkedListStack<VOStation> pilaStations = new LinkedListStack<>();
	LinkedListQueue<VOStation> colaStations = new LinkedListQueue<>();

	LinkedListStack<VOTrip> pilaTrips = new LinkedListStack<>();
	LinkedListQueue<VOTrip> colaTrips = new LinkedListQueue<>();
	
	LinkedListStack<VOTrip> pilaTripsQ2 = new LinkedListStack<>();


	public void loadTripsQ2(String tripsFile){
		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(tripsFile));
			String[] nextLine;
			try {
				nextLine = reader.readNext();
				nextLine = reader.readNext();
				while(nextLine != null){
					int trip_id;
					String start_time;
					String end_time;
					int bikeId;
					double tripDuration;
					int from_station_id;
					String from_station_name;
					int to_station_id;
					String to_station_name;
					String userType;
					String gender;
					int birthyear;

					if(nextLine[0] != ""){
						trip_id = Integer.parseInt(nextLine[0]);
					}
					else{
						trip_id = 0;
					}
					start_time = nextLine[1];
					end_time = nextLine[2];

					if(nextLine[3] != ""){
						bikeId = Integer.parseInt(nextLine[3]);
					}
					else{
						bikeId = 0;
					}
					if(nextLine[4] != ""){
						tripDuration = Double.parseDouble(nextLine[3]);
					}
					else{
						tripDuration = 0;
					}
					if(nextLine[5] != ""){
						from_station_id = Integer.parseInt(nextLine[5]);
					}
					else{
						from_station_id = 0;
					}

					from_station_name = nextLine[6];

					if(nextLine[7] != ""){
						to_station_id = Integer.parseInt(nextLine[7]);
					}
					else{
						to_station_id = 0;
					}

					to_station_name = nextLine[8];
					userType = nextLine[9];

					if(userType == "Subscriber"){
						gender = nextLine[10];
						birthyear = Integer.parseInt(nextLine[11]);
					}
					else{
						gender = "";
						birthyear = 0;
					}

					VOTrip trip = new VOTrip(trip_id, start_time, end_time, bikeId, 
							tripDuration, from_station_id, from_station_name, to_station_id, 
							to_station_name, userType, gender, birthyear);

					Node<VOTrip> nodeTrip = new Node<VOTrip>(trip);

					pilaTripsQ2.push(trip);

					nextLine= reader.readNext();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}					

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("no se encontr� el archivo");
			e1.printStackTrace();
		}
		System.out.println("El n�mero de viajes cargados fue: " + pilaTripsQ2.size());
	}
	public void loadStations (String stationsFile) {

		CSVReader reader;
		try{
			reader = new CSVReader(new FileReader(stationsFile));
			String[] nextLine;
			try {
				nextLine = reader.readNext();
				nextLine = reader.readNext();
				while(nextLine != null){
					int id;
					String name;
					String city;
					double latitude;
					double longitude;
					int dpcapacity;
					String online_date;

					if(nextLine[0] != ""){
						id = Integer.parseInt(nextLine[0]);
					}
					else{
						id = 0;
					}

					name = nextLine[1];
					city = nextLine[2];

					if(nextLine[3] != ""){
						latitude = Double.parseDouble(nextLine[3]);
					}
					else{
						latitude = 0;
					}

					if(nextLine[4] != ""){
						longitude = Double.parseDouble(nextLine[4]);
					}
					else{
						longitude = 0;
					}

					if(nextLine[5] != ""){
						dpcapacity = Integer.parseInt(nextLine[5]);
					}
					else{
						dpcapacity = 0;
					}

					online_date = nextLine[6];

					VOStation station = new VOStation(id, name, city, latitude, longitude, dpcapacity, online_date);

					Node<VOStation> nodeStation = new Node<VOStation>(station);

					pilaStations.push(nodeStation.getItem());
					colaStations.enqueue(nodeStation.getItem());

					nextLine = reader.readNext();
				}



			} catch (IOException e) {
				e.printStackTrace();
			}					

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("no se encontr� el archivo");
			e1.printStackTrace();
		}


	}

	public void loadTrips (String tripsFile)  {

		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(tripsFile));
			String[] nextLine;
			try {
				nextLine = reader.readNext();
				nextLine = reader.readNext();
				while(nextLine != null){
					int trip_id;
					String start_time;
					String end_time;
					int bikeId;
					double tripDuration;
					int from_station_id;
					String from_station_name;
					int to_station_id;
					String to_station_name;
					String userType;
					String gender;
					int birthyear;

					if(nextLine[0] != ""){
						trip_id = Integer.parseInt(nextLine[0]);
					}
					else{
						trip_id = 0;
					}
					start_time = nextLine[1];
					end_time = nextLine[2];

					if(nextLine[3] != ""){
						bikeId = Integer.parseInt(nextLine[3]);
					}
					else{
						bikeId = 0;
					}
					if(nextLine[4] != ""){
						tripDuration = Double.parseDouble(nextLine[3]);
					}
					else{
						tripDuration = 0;
					}
					if(nextLine[5] != ""){
						from_station_id = Integer.parseInt(nextLine[5]);
					}
					else{
						from_station_id = 0;
					}

					from_station_name = nextLine[6];

					if(nextLine[7] != ""){
						to_station_id = Integer.parseInt(nextLine[7]);
					}
					else{
						to_station_id = 0;
					}

					to_station_name = nextLine[8];
					userType = nextLine[9];

					if(userType == "Subscriber"){
						gender = nextLine[10];
						birthyear = Integer.parseInt(nextLine[11]);
					}
					else{
						gender = "";
						birthyear = 0;
					}

					VOTrip trip = new VOTrip(trip_id, start_time, end_time, bikeId, 
							tripDuration, from_station_id, from_station_name, to_station_id, 
							to_station_name, userType, gender, birthyear);

					Node<VOTrip> nodeTrip = new Node<VOTrip>(trip);

					pilaTrips.push(trip);
					colaTrips.enqueue(trip);

					nextLine= reader.readNext();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}					

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("no se encontr� el archivo");
			e1.printStackTrace();
		}
	}

	@Override
	public LinkedListQueue<String> getLastNStations(int bicycleId, int n) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public VOTrip customerNumberN(int stationID, int n) {
		// TODO Auto-generated method stub
		return null;
	}
		
	public VOTrip[] muestraNTrips(int n){
		VOTrip[] muestra = new VOTrip[n];
		int quedan = pilaTripsQ2.size() - n;
		for(int i=0; i<n; i++){
			muestra[i] = pilaTripsQ2.pop();
		}
		return muestra;
	}
	
	


}
