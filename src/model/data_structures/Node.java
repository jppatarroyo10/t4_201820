package model.data_structures;

public class Node<E> 
{
	private Node<E> next;
	private E item;
	
	public Node(E itm)
	{
		next = null;
		item = itm;
	}
	
	public Node<E> getNext()
	{
		return next;
	}
	
	public void setNext(Node<E> net)
	{
		next = net;
	}
	
	public E getItem()
	{
		return item;
	}
	
	public void setItem(E itm)
	{
		item = itm;
	}

}
