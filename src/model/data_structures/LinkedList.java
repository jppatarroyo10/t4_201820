package model.data_structures;

public class LinkedList<T> {
	private Node<T> first;
	private Node<T> actual;
	private int size;

	public LinkedList()
	{
		first = null;
	}

	public LinkedList(T item)
	{
		first = new Node<T>(item);
		actual=first;
		size = 1;
	}
	 public T getActual(){
		 return actual.getItem();
	 }
	 
	 public T getNextItem(){
		 return actual.getNext().getItem();
	 }
	public void addFirst(T item)
	{
		Node<T> newFirst = new Node<T>(item);
		newFirst.setNext(first);
		first = newFirst;
		size++;
	}

	public void addLast(T item)
	{
		Node<T> newLast = new Node<T>(item);
		if(first == null)
		{
			first = newLast;
		}
		else
		{
			Node<T> actual = first;
			while(actual.getNext()!= null)
			{
				actual = actual.getNext();
			}
			actual.setNext(newLast);
		}
		size++;
	}

	public void add(T item, int pos) throws Exception
	{
		Node<T> newNode = new Node<T>(item);
		int contador = 0;

		if(first == null)
		{
			throw new Exception("La lista est� vac�a");
		}
		
		else if( pos > size + 1){
			throw new Exception("No existe la posici�n dada");
		}

		else
		{
			if(pos == 1){
				addFirst(item);
				
			}
			else if(pos == size + 1){
				addLast(item);
			
			}
			else{
				Node<T> actual = first;
				while(actual.getNext()!= null )
				{
					contador++;
					if(contador+1 == pos)
					{
						newNode.setNext(actual.getNext());
						actual.setNext(newNode);
						size++;		
					}
					actual=actual.getNext();
				}				
			}
		}		
	}

	public void removeFirst() throws Exception
	{
		if(first == null)
		{
			throw new Exception("La lista est� vac�a");
		}
		else
		{
			Node<T> actual = first;
			first = first.getNext();
			actual.setNext(null);
			size--;
		}
	}

	public void removeLast() throws Exception
	{
		if(first == null)
		{
			throw new Exception("La lista est� vac�a");
		}
		else
		{
			Node<T> actual = first;
			while(actual.getNext().getNext() != null)
			{
				actual = actual.getNext();
			}
			actual.setNext(null);
			size--;
		}
	}

	public void remove(int pos) throws Exception
	{
		int contador = 0;
		if(first == null)
		{
			throw new Exception("La lista est� vac�a");
		}
		else
		{
			if(pos == 1){
				removeFirst();
			}
			else if(pos == size){
				removeLast();
			}
			else{
				Node<T> actual = first;
				while(actual.getNext() != null)
				{
					contador++;
					if(contador+1 == pos)
					{
						Node<T> nextNull = actual.getNext();
						actual.setNext(actual.getNext().getNext());
						nextNull.setNext(null);
						size--;
					}
					actual = actual.getNext();
				}
				throw new Exception("No se encontr� la posici�n dada");
			}
		}
	}

	public int getSize()
	{
		return size;
	}
	
	public T getFirst(){
		return first.getItem();
	}
	
	public T getItemN(int pos) throws Exception{
		int contador = 0;
		if( first == null){
			throw new Exception("la lista est� vac�a");
		}
		else{
			Node<T> actual = first;
			while(actual != null){
				contador++;
				if(contador == pos){
					return actual.getItem();
				}
				actual = actual.getNext();
			}
			throw new Exception("No existe un nodo en la posici�n dada");
		}	
	}
	
	public Node<T> getNodeN(int pos) throws Exception{
		int contador = 0;
		if( first == null){
			throw new Exception("la lista est� vac�a");
		}
		else{
			Node<T> actual = first;
			while(actual != null){
				contador++;
				if(contador == pos){
					return actual;
				}
				actual = actual.getNext();
			}
			throw new Exception("No existe un nodo en la posici�n dada");
		}	
	}
	
	public void changeItem(Node<T> elem1, Node<T> elem2){
		T item1 = elem1.getItem();
		elem1.setItem(elem2.getItem());
		elem2.setItem(item1);
	}

}
