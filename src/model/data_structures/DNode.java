package model.data_structures;

public class DNode<E> {
	
	private DNode<E> next;
	private DNode<E> previous;
	private E item;
	
	public DNode(E item, DNode<E> next, DNode<E> previous){
		this.item = item;
		this.next = next;
		this.previous = previous;
	}

	public DNode<E> getNext() {
		return next;
	}

	public void setNext(DNode<E> next) {
		this.next = next;
	}

	public DNode<E> getPrevious() {
		return previous;
	}

	public void setPrevious(DNode<E> previous) {
		this.previous = previous;
	}

	public E getItem() {
		return item;
	}

	public void setItem(E item) {
		this.item = item;
	}
	
	
}
