// Implementada con ayuda del libro "Data Structures and Algorithms 4th edition"
package model.data_structures;

public class DoubleLinkedList<T> {

	private int size;
	private DNode<T> header;
	private DNode<T> trailer;

	public DoubleLinkedList(){
		size = 0;
		header = new DNode<T>(null, null, null);
		trailer = new DNode<T>(null, null, header);
		header.setNext(trailer);
	}

	public int getSize(){
		return size;
	}

	public boolean isEmpty(){
		return size==0;
	}

	public DNode<T> getFirst() throws Exception{
		if(isEmpty()){
			throw new Exception("Lista vac�a");
		}
		else{
			return header.getNext();
		}
	}

	public DNode<T> getLast() throws Exception{
		if(isEmpty()){
			throw new Exception("Lista vac�a");
		}
		else{
			return trailer.getPrevious();
		}
	}

	public DNode<T> getPrevious(DNode<T> actual) throws Exception{
		if(actual == header){
			throw new Exception("No hay anterior");
		}
		else{
			return actual.getPrevious();
		}
	}

	public DNode<T> getNext(DNode<T> actual) throws Exception{
		if(actual == trailer){
			throw new Exception("No hay siguiente");
		}
		else{
			return actual.getNext();
		}
	}

	public void addBefore(DNode<T> nuevo, DNode<T> actual) throws Exception{
		if(actual == header){
			throw new Exception("No puede haber elemento anterior");
		}
		else{
			DNode<T> before = getPrevious(actual);
			nuevo.setNext(actual);
			nuevo.setPrevious(before);
			actual.setPrevious(nuevo);
			before.setNext(actual);
			size++;
		}
	}

	public void addAfter(DNode<T> nuevo, DNode<T> actual) throws Exception{
		if(actual == trailer){
			throw new Exception("No puede haber elemento siguiente");
		}
		else{
			DNode<T> after = getNext(actual);
			nuevo.setNext(after);
			nuevo.setPrevious(actual);
			actual.setNext(nuevo);
			after.setPrevious(nuevo);
			size++;
		}
	}

	public void addFirst(DNode<T> nuevo){
		try {
			addAfter(nuevo, header);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addLast(DNode<T> nuevo){
		try {
			addBefore(nuevo, trailer);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void remove(DNode<T> actual) throws Exception{
		if(actual == header || actual == trailer){
			throw new Exception("No se puede eleminar ese elemento");
		}
		else{
			DNode<T> before = getPrevious(actual);
			DNode<T> after = getNext(actual);
			
			after.setPrevious(before);
			before.setPrevious(after);
			actual.setPrevious(null);
			actual.setNext(null);
			size--;
		}
	}
	
	public boolean hasPrevious(DNode<T> actual){
		return actual.getPrevious() != header;
	}
	
	public boolean	hasNext(DNode<T> actual){
		return actual.getNext() != trailer;
	}
	
	public void changeItem(DNode<T> e1, DNode<T> e2){
		T item1 = e1.getItem();
		e1.setItem(e2.getItem());
		e2.setItem(item1);
	}


}
