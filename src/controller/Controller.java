package controller;

import api.IDivvyTripsManager;
import model.data_structures.LinkedListQueue;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static DivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadTripsQ2() {
		manager.loadTripsQ2("C:/Users/Juan Pablo Patarroyo/Desktop/JUANPA/6TO SEMESTRE/ESTRUCTURAS DE DATOS/Talleres/t4_201820/data/Divvy_Trips_2017_Q1Q2/Divvy_Trips_2017_Q2.csv");
	}
	
	public static void loadTrips() {
		manager.loadTrips("./data/Divvy_Trips_2017_Q3Q4/Divvy_Trips_2017_Q3.csv");	
	}
	
	public static VOTrip[] getMuestraNTrips(int n){
		return manager.muestraNTrips(n);
	}
		
	public static LinkedListQueue <String> getLastNStations (int bicycleId, int n) {
		return manager.getLastNStations(bicycleId, n);
	}
	
	public static VOTrip customerNumberN (int stationID, int n) {
		return manager.customerNumberN(stationID, n);
	}
}
